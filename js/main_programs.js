'use strict';

let idElem = document.getElementById("project-id");
let nameElem = document.getElementById("owner-name");
let titleElem = document.getElementById("title");
let catElem = document.getElementById("category");
let hourElem = document.getElementById("hours");
let rateElem =document.getElementById("rate");
let statusElem = document.getElementById("status");
let descrElem = document.getElementById("description");

let regexProjId = /^proj\d+$/;
let regexName = /^\w{1,}.*[^0-9]+.*/;
let regexTitle = /^\w{5,}/;
let regexCat = /^[^s]+/;
let regexHours = /^[1-9]+$/;
let regexRate = /^[1-9]+$/;
let regexStatus = /^[^s]+/;
let regexDescr = /^.*\w{3,}.*/;

let project_object ={};
let project_array = [];
let counter_delete = 1;


//blur element for the required fields
document.getElementById("project-id").addEventListener('blur', () => {
    setElementFeedBack(idElem, regexProjId);
    validateElement(idElem, regexProjId);
    enable_disable_Button();
});

//blur element for the required fields
document.getElementById("owner-name").addEventListener('blur', () => {
    setElementFeedBack(nameElem, regexName);
    validateElement(nameElem, regexName);
    enable_disable_Button();
});

//blur element for the required fields
document.getElementById("title").addEventListener('blur', () => {
    setElementFeedBack(titleElem, regexTitle);
    validateElement(titleElem, regexTitle);
    enable_disable_Button();
});

//blur element for the required fields
document.getElementById("category").addEventListener('blur', () => {
    setElementFeedBack(catElem, regexCat);
    validateElement(catElem, regexCat);
    enable_disable_Button();
});

//blur element for the required fields
document.getElementById("hours").addEventListener('blur', () => {
    setElementFeedBack(hourElem, regexHours);
    validateElement(hourElem, regexHours);
    enable_disable_Button();
});

//blur element for the required fields
document.getElementById("rate").addEventListener('blur', () => {
    setElementFeedBack(rateElem, regexRate);
    validateElement(rateElem, regexRate);
    enable_disable_Button();
});

//blur element for the required fields
document.getElementById("status").addEventListener('blur', () => {
    setElementFeedBack(statusElem, regexStatus);
    validateElement(statusElem, regexStatus);
    enable_disable_Button();
});

//blur element for the required fields
document.getElementById("description").addEventListener('blur', () => {
    setElementFeedBack(descrElem, regexDescr);
    validateElement(descrElem, regexDescr);
    enable_disable_Button();
});


//adding an addeventlistener to the reset button
document.getElementById("reset").addEventListener("click", resetButton);
function resetButton() {
    let spanLength = document.getElementsByClassName("req").length;
    for (let i = 0 ; i < spanLength ; i++) {
        document.querySelector(".req").remove();
    }
    
    let formName = document.getElementById("top-form");
    formName.reset();

}


 